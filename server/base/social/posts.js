const stream = require('getstream')
const client = stream.connect('fp3j44zrkhbj', 'sabz57sgem4nafaamvbprmuujwtrs3yg3736x5xpwz7jvk5qp9pk7688cra94hz2')
const Boom = require('boom')
const ActsModel = require('../schema/post-schema').Act

class Posts {
  constructor (me) {
    if (!me) {
      throw Boom.badRequest('Constructor expects one argument which')
    }
    this.me = me
    this.myUserFeed = client.feed('user', this.me)
    this.myNotificationFeed = client.feed('notification', this.me)
  }
  createActivityRecord (actData) {
    return new Promise((resolve, reject) => {
      let a = new ActsModel(actData)
      a.save((err, aDone) => {
        if (err) {
          return reject(err)
        }
        resolve(aDone)
      })
    })
  }
  getFeed () {
    let me = client.feed('user', this.me)
    return me.get()
    // .then((results) => {
    //   // var activityData = results
    //   reply(results.results)
    // })
    // .catch(err => {
    //   reply(err)
    // })
  }
  /**
   * saves an activity to the stream.io activity
   * feed. After successfully saving on stream.io,
   * we keep a record on MongoDB using
   * `createActivityRecord` and return the
   * object from stream.io
   * @param  {Object} requestBody JSON payload / body from the client
   * @param {String} feedName name of the feed setup at getstream.io
   * @return {Promise}         [description]
   */
  postToFeed (savedPosts, feedName = 'user') {
    const self = this
    return new Promise((resolve, reject) => {
      let me = client.feed(feedName, self.me)
      let activity = {}
      // for (let w in savedPosts) {
      //   if (savedPosts.hasOwnProperty(w)) {
      //     activity[w] = savedPosts[w]
      //   }
      // }
      activity.object = `${savedPosts.post_type}:${savedPosts.docId}`
      activity.actor = `user:${self.me}`
      activity.verb = savedPosts.post_type
      activity.foreign_id = `${activity.verb}:${self.me}`
      if (savedPosts.to) {
        activity.to = [savedPosts.to]
      }
      var now = new Date()
      activity.time = now.toISOString()
      me.addActivity(activity)
      .then(success => {
        // save in our db
        self.createActivityRecord(activity)
        .then(() => {
          resolve(success)
        }, err => {
          reject(err)
        })
      }, err => {
        reject(err)
      })
      .catch(err => {
        reject(err)
      })
    })
  }
}

module.exports = Posts
