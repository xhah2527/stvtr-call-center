const Posts = require('./social/posts')
const Boom = require('boom')
const Joi = require('joi')
const Crud = require('../lib/crud')
const _ = require('lodash')

// Base routes for default index/root path, about page, 404 error pages, and others..
exports.register = function (server, options, next) {
  const api = server.select('api')

  api.realm.modifiers.route.prefix = '/api/v1'
  api.route([
    {
      method: 'POST',
      path: '/tasks',
      config: {
        // validate: {
        //   payload: {
        //     description: Joi.string(),
        //     lga_id: Joi.string(),
        //     state_id: Joi.string(),
        //     body: Joi.string().min(1),
        //     task_amount: Joi.number(),
        //     postDate: Joi.date(),
        //     attachments: [Joi.array()],
        //     post_type: Joi.string().required()
        //   }
        // }
      },
      handler: (request, reply) => {
        const posts = new Posts(request.auth.credentials.uid)
        let crudDo = new Crud('posts')
        let savedPosts = request.payload
        crudDo.create(savedPosts)
        .then((idDocument) => {
          if (!savedPosts.postSocial) return reply(idDocument)
          // the ObjectId of the actual comment
          savedPosts.docId = idDocument._id.toString()
          // use this to send a notification about
          // this comment to the post author
          savedPosts.to = `user:${savedPosts.post_author}`
          savedPosts.verb = 'feed-post'
          posts.postToFeed(savedPosts)
          .then(done => {
            reply(done)
          }, err => {
            reply(err)
          })
          .catch(err => {
            reply(err)
          })
        }, err => {
          reply(Boom.badRequest('invalid op', err))
        })
      }
    },
    {
      method: 'GET',
      path: '/tasks',
      handler: (request, reply) => {
        let posts = new Crud('posts')
        posts.collectionInstance.find({
          'post_type': 'micro-task',
          'post_status': 'published'
        })
        .exec((err, p) => {
          if (err) {
            return reply(Boom.badRequest(err))
          }
          reply(p.map(postDb => {
            postDb.post_content.taskId = postDb._id
            return postDb.post_content
          }))
        })
        // reply([{}])
      }
    },
    {
      method: 'DELETE',
      path: '/tasks/{taskId}',
      handler: (request, reply) => {
        let posts = new Crud('posts')
        posts.deletePost(request.params.taskId)
        .exec((err, p) => {
          if (err) {
            return reply(Boom.badRequest(err))
          }
          reply()
        })
      }
    },
    {
      method: 'PUT',
      path: '/tasks/{taskId}',
      config: {
        validate: {
          payload: {
            bidId: Joi.string().required(),
            taskId: [Joi.string().required()]
          }
        }
      },
      handler: (request, reply) => {
        let posts = new Crud('posts')
        let Postmodel = posts.collectionInstance
        Postmodel.update({
          _id: request.params.taskId
        }, {
          'post_content.assignedTo': request.payload.bidId
        })
        .exec((err, done) => {
          if (err) {
            return reply(Boom.badRequest(err))
          }
          if (done.nModified) {
            reply()
          } else {
            reply(Boom.conflict('possible you are trying to repeat an operation you can only carry out one time.'))
          }
        })
      }
    },
    {
      method: 'POST',
      path: '/tasks/{taskId}/bids',
      handler: ((request, reply) => {
        let savedPost = request.payload
        savedPost.post_author = request.auth.credentials.uid
        savedPost.post_status = 'published'
        let post = new Crud('posts')
        post.create(savedPost)
        .then(d => {
          reply(d)
        }, err => {
          reply(Boom.badRequest(err))
        })
      }),
      config: {
        validate: {
          payload: {
            taskId: Joi.string().required(),
            amount: Joi.number().required(),
            duration: [Joi.number()],
            bidMessage: [Joi.string()],
            post_type: Joi.string().required(),
            prop: [Joi.string()]
          }
        }
      }
    },
    {
      method: 'GET',
      path: '/tasks/{taskId}/bids',
      config: {
        validate: {
          params: {
            taskId: Joi.string().required()
          }
        }
      },
      handler: (request, reply) => {
        // if ()
        let posts = new Crud('posts')
        posts.collectionInstance.findOne({
          'post_type': 'micro-task',
          _id: request.params.taskId
        })
        .exec((err, b) => {
          if (err) {
            return reply(Boom.badRequest(err))
          }
          // if
          posts.collectionInstance.find({
            'post_type': 'bid',
            'post_content.taskId': request.params.taskId 
          })
          .exec((err, bids) => {
            if (err) {
              return reply(err)
            }
            b = b.toObject()
            b.post_content.bids = _.map(bids, cont => {
              cont.post_content.bidId = cont._id
              cont.post_content.createdAt = cont.post_date
              return cont.post_content
            })
            reply(b.post_content)
          })
        })
      }
    },
    {
      method: 'DELETE',
      path: '/bids/{bidId}',
      handler: (request, reply) => {
        let bid = new Crud('posts')
        bid.collectionInstance.update({
          _id: request.params.bidId
          
        }, {
          'post_status': 'deleted'
        })
        .exec((err, b) => {
          if (err) {
            return reply(Boom.badRequest(err))
          }
          if (b.nModified) {
            reply()
          } else {
            reply(Boom.conflict('This seems like an unnecessary operation.'))
          }
        })
      }
    },
    {
      method: 'GET',
      path: '/bids/{bidId?}',
      handler: (request, reply) => {
        let bids = new Crud('posts')
        bids.collectionInstance.find({
          'post_type': 'bid',
          'post_author': request.auth.credentials.uid,
          'post_status': 'published'
        })
        .exec((err, b) => {
          if (err) {
            return reply(Boom.badRequest(err))
          }
          let x = b.map(cont => {
            cont.post_content.bidId = cont._id
            cont.post_content.createdAt = cont.post_date            
            return cont.post_content
          })
          let y = []
          function loopTaskId () {
            if (!x.length) return reply(y.length ? y : x)
            let currentItem = x.pop()
            const crud = new Crud('posts')
            crud.collectionInstance.findOne({
              _id: currentItem.taskId,
              post_type: 'micro-task'
            }, 'post_content')
            .exec((err, mic) => {
              if (err) {
                console.log(err)
              }
              currentItem.taskData = mic ? mic.post_content : {}
              y.push(currentItem)
              loopTaskId()
            })
          }
          loopTaskId()
        })
      }
    }
  ])

  next()
}

exports.register.attributes = {
  name: 'tasks'
}
