const ChatSessionModel = require('./schema/chat-schema').ChatSession
const UserModel = require('./schema/user-schema')
const Boom = require('boom')
const chatFuncs = {
  instigateChat: function (me, padi) {
    let session = new ChatSessionModel()
    session.instigator = me
    session.participants.push(me)
    session.participants.push(padi)
    session.padi = padi
    return session.save()
  }
}

exports.register = (server, options, next) => {
  const api = server.select('api')

  api.realm.modifiers.route.prefix = '/api/v1'
  api.route([
    /**
     * this route will initiate a chat seesion
     * for the currently logged in user and the
     * user designated by `padiId`
     * The logic behind this is, to check if
     * there already exists a chat session initiated
     * by the user `padiId`. Else, we initiate on with
     * this currently auth'ed user
     */
    {
      method: 'GET',
      path: '/chat/{padiId}',
      handler: (request, reply) => {
        // initially checking if the user `padiId`
        // has created a session before
        ChatSessionModel.findOne({
          // instigator: request.params.padiId,
          $and: [
            {
              participants: {
                $in: [request.auth.credentials.uid]
              }
            },
            {
              participants: {
                $in: [request.params.padiId]
              }
            }]
        })
        .lean()
        .sort({created_at: -1})
        .exec((err, session) => {
          if (err) {
            return reply(Boom.badRequest('invalid operation', err))
          }
          UserModel.findOne({
            userId: request.params.padiId
          })
          .lean()
          .exec()
          .then(userPadi => {
            if (!session) {
              // if session isnt found,
              // lets instigate a chat with
              // this padi
              chatFuncs.instigateChat(request.auth.credentials.uid, request.params.padiId)
              .then(newSession => {
                newSession = newSession.toObject()
                newSession.padiData = userPadi
                reply(newSession)
              }, err => {
                reply(err)
              })
            } else {
              session.padiData = userPadi
              reply(session)
            }
          }, err => {
            reply(Boom.badRequest('bad', err))
          })
        })
      }
    }
  ])
  next()
}
exports.register.attributes = {
  name: 'chat'
}

