const Request = require('request')
const path = require('path')
const fs = require('fs')
const _ = require('lodash')
const VoiceResponse = require('twilio').twiml.VoiceResponse;
const stringTemplate = require('string-template')
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter)
if (!process.env.TW_ACCOUNT_SID || !process.env.TW_AUTH_TOKEN) throw new Error('set env vars');
const accountSid = process.env.TW_ACCOUNT_SID;
const authToken = process.env.TW_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);


// Set some defaults (required if your JSON file is empty)
db.defaults({
  "accountProfile" : [],
  "beneficiaryProfile" : [],
  "transfers" : [],
  "verbals" : {
    
  }
}).write()


const accountProfileSchema  = {
  "title": "Account Profile",
  "description": "A users bank account.",
  "type": "object",
  "required": [
    "firstName",
    "lastName",
    "account_number"
  ],
  "uiSchema" : {
    "password" : {
      "ui:widget" : "password"
    },
    "record_type" : {
      "ui:widget" : "hidden"
    }
  },
  "properties": {
    "firstName": {
      "type": "string",
      "title": "First name"
    },
    "lastName": {
      "type": "string",
      "title": "Last name"
    },
    "account_number": {
      "type": "number",
      "title": "Account Number",
      "maxLength" : 10,
      "minLength" : 5
    },
    "pin": {
      "type": "number",
      "title": "PIN",
      "maxLength" : 4,
      "minLength" : 4
    },
    "email": {
      "type": "string",
      "title": "Email Address"
    },
    "current_balance": {
      "type": "number",
      "title": "Current Balance"
    },
    "customerId": {
      "type": "number",
      "title": "Customer Phonebanking ID"
    },
    "password": {
      "type": "string",
      "title": "Password",
      "minLength": 3
    },
    "record_type": {
        "type" : "string",
        "default" : "profile"
    },
    "telephone": {
      "type": "string",
      "title": "Telephone",
      "minLength": 10
    },
    "currency": {
      "type" : "string",
      "title": "Account Currency"
    }
  }
}

const accountBeneficiarySchema = {
  "title": "Account Beneficiaries",
  "description": "for outgoing transfers",
  "type": "object",
  "uiSchema" : {
   
  },  
  "properties" : {
    "account_number" : {
      "type": "string",
      "title" : "Account Number"
    },
    "phone_number" : {
      "type": "string",
      "title" : "Phone Number"
    },    
    "routing_number" : {
      "type": "number",
      "title": "Routing Number"
    },
    "bank_name": {
      "type": "string",
      "title": "Bank Name"
    },
    "account_name": {
      "type": "string",
      "title": "Account Name"
    }
  }
}

const transferSchema = {
  source_account: {
    
  },
  beneficiary_account: {
    
  },
  amount: '',
  previous_balance: '',
  transfer_balance: '',
  transaction_date: ''
}

// function prepareProperties (d) {
//   let k = Object.keys(d)
//   let props = {}
//   k.forEach(key => {
//     props[key] = {
//       "type": "string",
//       "title": key
//     }
//   })
//   return props
// }


let default_verbs = {
  "start_transfer_request" : `Beneficiary Found. \n 
              Account Name is {person_account_name} and the routing number is {person_routing_number} \n 
              Enter the amount you wish to transfer to {person_account_name} and press the # key to send.`, 
  "start_transfer_request_lookup_failed" : `Look up failed. Please add this account number to your beneficiaries list before you can make a phone transfer.`,
  "process_transfer_error_1":  `We are sorry, we cannot complete this request. Good bye`,
  "process_transfer_error_2" : `We are sorry, we cannot complete this request. Good bye`,
  "process_transfer_pre_confirm" : `You are now transfering {tf_amount} US Dollars to {person_account_name}`,
  "process_transfer_post_confirm" : `Your transfer is complete. \n Your balance is {oldProfile_current_balance}. \n Thank you.`,
  "account_balance" : `Your checking account balance is {profiles_current_balance} {profiles_currency}`,
  "validate_beneficiary" : `Enter in your authorized beneficiary account number to validate and press # when you are done.`,
  "profile_login_success" : `Login successful. \n \n
            Good morning {profiles_firstName} \n
            Please press 3 to listen to your account balance or ,
            Press 4 to make a transfer
            ` ,
            
 "profile_login_error" :  'Unsuccessful Login attempt. Good bye',
 "enter_pin_prompt" : `Now you need to enter your 4 digit PIN.`,
 "enter_account_error_1" : 'You did not enter a valid account number. \n Good bye',
 "enter_account_error_2" : 'You did not enter a valid account number. \n Good bye',
 "dial_home" : 'For account information, press 1. For support, press 2.',
 "you_need_to_login_greeting" : `You need to log in to proceed. \nWhat is your phone banking I.D or your enter your account number. Type it in and press # to submit`,
 "agents_busy" : 'All our agents are currently busy. Please hold.',
 "sms_confirmation" : "Your balance is {oldProfile_current_balance}."
}


const verbalSchema = {
    "title": "Call Voice Prompts",
    "description": "for changing what is said",
    "type": "object",
    "uiSchema": {},
    "properties": {
        "start_transfer_request": {
            "type": "string",
            "title": "start_transfer_request"
        },
        "process_transfer_error_1": {
            "type": "string",
            "title": "process_transfer_error_1"
        },
        "process_transfer_error_2": {
            "type": "string",
            "title": "process_transfer_error_2"
        },
        "process_transfer_pre_confirm": {
            "type": "string",
            "title": "process_transfer_pre_confirm"
        },
        "process_transfer_post_confirm": {
            "type": "string",
            "title": "process_transfer_post_confirm"
        },
        "account_balance": {
            "type": "string",
            "title": "account_balance"
        },
        "validate_beneficiary": {
            "type": "string",
            "title": "validate_beneficiary"
        },
        "profile_login_success": {
            "type": "string",
            "title": "profile_login_success"
        },
        "profile_login_error": {
            "type": "string",
            "title": "profile_login_error"
        },
        "enter_pin_prompt": {
            "type": "string",
            "title": "enter_pin_prompt"
        },
        "enter_account_error_1": {
            "type": "string",
            "title": "enter_account_error_1"
        },
        "enter_account_error_2": {
            "type": "string",
            "title": "enter_account_error_2"
        },
        "dial_home": {
            "type": "string",
            "title": "dial_home"
        },
        "you_need_to_login_greeting": {
            "type": "string",
            "title": "you_need_to_login_greeting"
        },
        "agents_busy": {
            "type": "string",
            "title": "agents_busy"
        },
        "sms_confirmation": {
            "type": "string",
            "title": "sms_confirmation"
        }
    }
}

function combine_verbs () {
  return Object.assign(
            default_verbs, db.get('verbals').value()
          )
}


// const root_url = 'https://tw-ivr-ninja-k1ngk0d3d.c9users.io'
const root_url = process.env.IPPORT


// Base routes for default index/root path, about page, 404 error pages, and others..
exports.register = function (server, options, next) {
  server.route([
    {
      method: 'POST',
      path: '/save-message',
      handler: (request, reply) => {
        let dbKey = Object.keys(request.payload)[0]
        // if there is something to change
        if (Object.keys(request.payload).length && request.payload[dbKey].length) {
          
          let verbsMessage = db.set(`verbals.${dbKey}`, request.payload[dbKey]).write()
          reply(verbsMessage.verbals[dbKey] === request.payload[dbKey]? true : false)
          
        } else if (Object.keys(request.payload).length && (request.payload[dbKey].length === 0)) {
          db.set(`verbals.${dbKey}`, default_verbs[dbKey]).write()
          reply(default_verbs[dbKey])
        }
      }
    },
    {
      method: 'GET',
      path: '/start-transfer/{ac_no}',
      handler: (request, reply) => {
        const twiml = new VoiceResponse();
        let accountNumber = request.params.ac_no
        let personId = parseInt(request.query.Digits)
        // confirm name
        if (accountNumber && accountNumber.length) {
          let person = db.get('beneficiaryProfile').find({'account_number': personId}).value()
          if (person && person._id) {
            let gat = twiml.gather({
              action: '/process-transfer/' + accountNumber + '/' + personId,
              method: 'GET'
            })
            let routing_number = person.routing_number + ""
            gat.say(stringTemplate(combine_verbs().start_transfer_request, {
                person_account_name: person.account_name,
                person_routing_number: routing_number.split("").join(" ")
              })
              )
          } else {
            twiml.say(combine_verbs().start_transfer_request_lookup_failed)
            twiml.redirect({
              method: 'GET'
            }, `${root_url}/start-transfer/${accountNumber}`)
            
          }
        } else {
            twiml.redirect({
              method: 'GET'
            }, `${root_url}/start-transfer/${accountNumber}`)
        }
        // read routing
        // request amount
        // confirm transfer
        reply(twiml.toString())
      }
    },
    {
      method: 'GET',
      path: '/process-transfer/{ac_no}/{personId}',
      handler: (request, reply) => {
        const twiml = new VoiceResponse();
        const tf_amount = request.query.Digits
        const person = db.get('beneficiaryProfile').find({'account_number':parseInt(request.params.personId) }).value()
        
        if (!person) {
          twiml.say(combine_verbs().profile_transfer_error_1)
          return reply(twiml.toString())
        }        
        
        const accountNumber = request.params.ac_no

        
        // do tf
        let oldProfile = db.get('accountProfile').find({
          'account_number' : parseInt(accountNumber)
        }).value()
        
        if (!oldProfile) {
          twiml.say(combine_verbs().profile_transfer_error_2)
          return reply(twiml.toString())
        }
        
        // confirming tf
        
        twiml.say(stringTemplate(
          combine_verbs().process_transfer_pre_confirm, {
              tf_amount,
              person_account_name: person.account_name
            }
          )
        )
        twiml.pause({
          length: 3
        })
        
        // recording tf
        let transferData = {
            source_account: accountNumber,
            beneficiary_account: person,
            amount: tf_amount,
            previous_balance: oldProfile.current_balance,
            transaction_date: Date.now()            
          }
        
        // substract the amounts
        oldProfile.current_balance = oldProfile.current_balance - tf_amount
        transferData.new_balance = oldProfile.current_balance 
        
        
        // account profile 
        db.get('accountProfile')
          .find({
            'account_number' : accountNumber
          })
          .set({
            'current_balance' : oldProfile.current_balance
          })
          .write()
          
        // transfer information
        db.get('transfers')
        .push(
            transferData
          )
        .write()
        
        twiml.say(stringTemplate(combine_verbs().process_transfer_post_confirm, {
              oldProfile_current_balance: oldProfile.current_balance
            }
          )
        )
        client.messages
          .create({
             body: stringTemplate(combine_verbs().sms_confirmation, {
               oldProfile_current_balance: oldProfile.current_balance
             }),
             from: '18443857566',
             to: person.phone_number
           })
          .then(message => console.log(message.sid))
          .done();
        twiml.pause({
          length: 3
        })        
        
        twiml.redirect({
          method: 'GET'
        }, '/process-input')
        reply(twiml.toString(3))
      }
    },
    {
      method: 'GET',
      path: '/process-account/{ac_no?}',
      handler: (request, reply) => {
        const accountNumber = parseInt(request.params.ac_no)
        const twiml = new VoiceResponse();
        if (parseInt(request.query.Digits) == 3) {
          let profiles = db.get('accountProfile').find({
            // 'record_type' : 'profile',
            'account_number' : accountNumber
          })
          .value()  
          if (!profiles) return reply(twiml.toString())
          twiml.say({
            loop: 4
          }, stringTemplate(
              combine_verbs().account_balance, {
                profiles_current_balance: profiles.current_balance,
                profiles_currency: profiles.currency
              }
            )
          )
          reply(twiml.toString())
        } else if (parseInt(request.query.Digits) == 4) {
          twiml.say('Hold on while we connect your beneficiaries')
          twiml.pause({
            length: 2
          })
          let gat = twiml.gather({
            action:  `${root_url}/start-transfer/${accountNumber}`,
            method: 'GET'
          })
          gat.say(combine_verbs().validate_beneficiary)
          
          reply(twiml.toString())
        } else {
          twiml.say(`That options is not avaiable.`)
          reply(twiml.toString())
        }
      }
    },
    {
      method: 'GET',
      path: '/verify-pin/{ac_no?}',
      handler: (request, reply) => {
        const accountNumber = parseInt(request.params.ac_no)
        const twiml = new VoiceResponse();
        let profiles = db.get('accountProfile').find({
          // 'record_type' : 'profile',
          'account_number' : accountNumber
        })
        .value()
        
        if (!profiles) return reply(twiml.toString())
        // compare request pin and db pin
        if (parseInt(request.query.Digits) == parseInt(profiles.pin)) {
          let gat = twiml.gather({
            action: `${root_url}/process-account/${accountNumber}`,
            method: 'GET',
            numDigits: 1
          })
          gat.say(stringTemplate(combine_verbs().profile_login_success, {
                profiles_firstName: profiles.firstName
              }
            )
          )
        } else {
          twiml.say(combine_verbs().profile_login_error)
        }
        
        reply(twiml.toString())
      }
    },
    {
      method: 'GET',
      path: '/get-account',
      config: {
        handler: (request, reply) => {
          let profiles
          let accountNumber = request.query.Digits
          const twiml = new VoiceResponse();
          if (accountNumber && accountNumber.length > 1) {
            profiles = db.get('accountProfile').find({
              // 'record_type' : 'profile',
              'account_number' : parseInt(accountNumber)
            })
            .value()
            // is valid account found
            if (profiles && profiles._id) {
              
              let gather = twiml.gather({
                method: 'GET',
                numDigits: 4,
                // send d account number to processing to login
                action: `${root_url}/verify-pin/${accountNumber}`
              })
              
              gather.say(combine_verbs().enter_pin_prompt)
            } else {
              
              twiml.say(combine_verbs().enter_account_error_1)
            }
            
            
          } else {
            twiml.say(combine_verbs().enter_account_error_2)
          }
          reply(twiml.toString())
        }
      }
    },
    {
      method: 'GET',
      path: '/get-beneficiary/{profileId?}',
      config: {
        handler: (request, reply) => {
          let profiles
          let accountNumber
          if (request.params.profileId && request.params.profileId.length) {
            accountNumber = request.query.digits || request.params.profileId
            profiles = db.get('beneficiaryProfile').find({
              // 'record_type' : 'profile',
              'account_number' : parseInt(accountNumber)
            })
            .value()
          } else {
            
              profiles = db.get('beneficiaryProfile').value()
          }
          reply(profiles || {})
        }
      }
    },
    {
      method: 'GET',
      path: '/get-profiles/{profileId?}',
      config: {
        handler: (request, reply) => {
          let profiles
          let accountNumber
          if (request.params.profileId && request.params.profileId.length) {
            accountNumber = request.query.digits || request.params.profileId
            profiles = db.get('accountProfile').find({
              // 'record_type' : 'profile',
              'account_number' : parseInt(accountNumber)
            })
            .value()
          } else {
            
              profiles = db.get('accountProfile').filter({
                  'record_type': 'profile'
                }
              ).value()
          }
          reply(profiles || {})
        }
      }
    },
    {
      method: 'POST',
      path: '/save-beneficiary',
      config: {
        payload:{
            output: 'data',
            parse:true
        },
        handler: (request, reply) => {
          let person = request.payload
          person._id = _.uniqueId()
          person.account_number = parseInt(person.account_number)
          db.get('beneficiaryProfile')
          .push(
            person
          )
          .write()
          
          reply({
            _id: person._id
          })
        }
      }
      
    },    
    {
      method: 'POST',
      path: '/save-profile',
      config: {
        payload:{
            output: 'data',
            parse:true
        },
        handler: (request, reply) => {
          let savings = request.payload
          savings._id = _.uniqueId()
          db.get('accountProfile')
          .push(
            savings
          )
          .write()
          
          reply({
            _id: savings._id
          })
        }
      }
      
    },
    {
      method: 'GET',
      path: '/get-schema/{schema?}',
      config: {
        handler: (request, reply) => {
          if (request.params.schema && request.params.schema == 'profile') {
            return reply(accountProfileSchema)
          } else if (request.params.schema && request.params.schema == 'beneficiary' ) {
            return reply(accountBeneficiarySchema)
          } else if (request.params.schema && request.params.schema == 'transfer' ) {
            return reply(transferSchema)
          } else if (request.params.schema && request.params.schema == 'verbals' ) {
            return reply(verbalSchema)
          }
          reply({
            "title": "No Data to display",
            "description": "click here to reload",
            "type": "object"
            
          })
        }
      }
    },
    {
      method: 'GET',
      path: '/dial-home',
      config: {
        handler: (request, reply) => {
          // Create TwiML response
          const twiml = new VoiceResponse();

          // Use the <Gather> verb to collect user input
          const gather = twiml.gather({ 
            numDigits: 1,
            action: `${root_url}/process-input`,
            method: 'GET'
          });
          gather.say(
            combine_verbs().dial_home
            );
          
          // If the user doesn't enter input, loop
          twiml.redirect({method: 'GET'}, '/dial-home');
          
      
          // twiml.say('Hello! We are sorry to inform you, this service is currently being upgraded. Good bye.');          
          reply(twiml.toString()) 
        }
      }
    },
    {
      method: 'GET',
      path: '/process-input',
      config: {
        handler: (request, reply) => {
          const twiml = new VoiceResponse();
          if (request.query.Digits == 1) {
            
            const gather = twiml.gather({
              method: 'GET',
              // send d account number to processing to login
              action: `${root_url}/get-account`
            })
            
            gather.say(combine_verbs().you_need_to_login_greeting)
            
          } else {
            twiml.say(combine_verbs().agents_busy);
            twiml.play({
                loop: 10
            }, 'https://api.twilio.com/cowbell.mp3');       
            
          }
          reply(twiml.toString())
        }
      }
    },
    {
      method: 'DELETE',
      path: '/delete-profile/{ac_no}',
      config: {
        
        handler: (request, reply) => {
          let act_profiles = db.get('accountProfile').remove({
                'account_number' : parseInt(request.params.ac_no)
              })
              .write()
          reply(act_profiles)
        }
      }
    },
    {
      method: 'DELETE',
      path: '/delete-beneficiary/{ac_no}',
      config: {
        handler: (request, reply) => {
          let act_profiles = db.get('beneficiaryProfile').remove({
                'account_number' :  parseInt(request.params.ac_no)
              })
              .write()
          reply(act_profiles)        
        }      
      }
    },
    {
      method: 'GET',
      path: '/get-verbals',
      handler: (request, reply) => {
        reply(
          Object.assign(
            default_verbs, db.get('verbals').value()
          )
        )
      }
    },
    {
      method: 'GET',
      path: '/lost-home',
      config: {
        handler: (request, reply) => {
          // Create TwiML response
          const twiml = new VoiceResponse();
      
          twiml.say('Have a good day and Good bye');          
          reply(twiml.toString()) 
        }
      }
    }
  ])

  next()
}

exports.register.attributes = {
  name: 'base'
}
