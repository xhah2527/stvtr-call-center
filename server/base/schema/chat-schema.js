const mongoose = require('mongoose')
const Schema = mongoose.Schema

let chatSchema = new Schema({
  created_at: {
    type: Date,
    default: Date.now()
  },
  padi: {
    type: String
  },
  participants: [{type: String}],
  archived: {
    type: Boolean
  },
  // The user who stated this chat session
  instigator: {
    type: String
  }
})

let messagesSchema = new Schema({
  createdAt: {
    type: Date,
    default: Date.now()
  },
  destUser: {
    type: String
  },
  fromUser: {
    type: String
  },
  message: {
    type: String
  },
  status: {
    type: String
  },
  chat_session_id: {
    type: Schema.Types.ObjectId
  }
})

module.exports.Messages = mongoose.model('message', messagesSchema)
module.exports.ChatSession = mongoose.model('chatsesssion', chatSchema)
