const mongoose = require('mongoose')
const Schema = require('mongoose').Schema

let PostSchema = new Schema({

  name: {
    type: String
  },
  post_title: {type: String},
  // the author or creator of this string.
  // mandatory
  post_author: {
    type: String,
    min: 1,
    required: true
  },
  post_date: {
    type: Date,
    default: Date.now(),
    required: true
  },
  post_content: {
    type: Schema.Types.Mixed
  },
  post_status: {
    type: String
  },
  post_type: {
    type: String,
    required: true
  },
  parent_id: {
    type: String
  },
  attachements: [{
    type: Schema.Types.Mixed
  }],
  likes: {
    type: Number,
    default: 0,
    min: 0
  }
})

let activitySchema = new Schema({
  actor: {
    type: String
  },
  verb: {
    type: String
  },
  object: {
    type: String
  },
  target: {
    type: String
  }
})

module.exports.Act = mongoose.model('act', activitySchema)

module.exports.Post = mongoose.model('posts', PostSchema)
