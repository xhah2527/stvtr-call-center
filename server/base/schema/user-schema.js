
const mongoose = require('mongoose')
const Schema = require('mongoose').Schema

function trimPhone (v) {
  return v.replace(/\D/g, '')
}

const userSchema = new Schema({
  email: {
    type: String
  },
  first_name: {
    type: String
  },
  surname: {
    type: String
  },
  phone: {
    type: String,
    set: trimPhone
  },
  userId: {
    type: String
  },
  displayName: {
    type: String
  },
  resume: {
    type: Schema.Types.Mixed,
    default: {
      fullname: 'None'
    }
  },
  photoUrl: {
    type: String
  }
})

userSchema.set('phone')


mongoose.model('user', userSchema)
module.exports = mongoose.model('user') 
