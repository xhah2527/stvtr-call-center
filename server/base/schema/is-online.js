const mongoose = require('mongoose')
const Schema = require('mongoose').Schema

let isOnline = new Schema({
  userId: {
    type: String
  },
  checkInTime: {
    type: Date
  },
  deviceId: {
    type: String
  }
})

mongoose.model('useronline', isOnline)
module.exports = mongoose.model('useronline') 
