const Users = require('./schema/user-schema')
const SocialUser = require('./social/user')
const Boom = require('boom')
const stream = require('getstream')
const client = stream.connect('fp3j44zrkhbj', 'sabz57sgem4nafaamvbprmuujwtrs3yg3736x5xpwz7jvk5qp9pk7688cra94hz2')
const Joi = require('joi')
// Base routes for default index/root path, about page, 404 error pages, and others..
exports.register = function (server, options, next) {
  const api = server.select('api')
  api.realm.modifiers.route.prefix = '/api/v1'
  api.route([
    /**
     * this api route GET/users will fetch users
     * on Emplug. This logic will fetch users that
     * match the currently logged in users social
     * and employment attributes.
     * Current testing assumptions this can be used
     * for: suggested pluggable users
     * Job Seeker -> Employers, Pluggers who have posted
     * opportunities etc.
     * Socializer -> Pluggers to associate with / plug to
     */
    {
      method: 'GET',
      path: '/users',
      handler: (request, reply) => {
        const users = new SocialUser(request.auth.credentials.uid)
        users.exploreUsersOnEmplug()
        .then((d) => {
          users.myUserFeed.following({offset: 0, limit: 10})
          .then(f => {
            let idzForMySheep = f.results.map(ele => {
              return ele.target_id.split(':')[1]
            })
            // every element in our list of users to explore
            d.forEach(function (element, index, dArray) {
              // we check if there is also a matching id
              // in the idzForMySheep
              if (idzForMySheep.indexOf(element.userId) > -1) {
                // if we find a match, tag that match true
                dArray[index] = dArray[index].toObject()
                dArray[index].uDeyFollowAm = true
              }
            }, this)
            // compare d to the users following, so we
            // know users he already follows

            reply(d)
          })
          .catch(err => {
            reply(err)
          })
          // then gimi d list of people / user feeds i follow.
        }, (err) => {
          reply(err)
        })
      }
    },
    {
      method: 'GET',
      path: '/users/{userId}',
      handler: function (request, reply) {
        Users.findOne({
          userId: request.params.userId
        })
        .then(userdocument => {
          reply(userdocument)
        }, err => {
          reply(Boom.wrap(err, 500))
        })
      },
      config: {
        description: 'gets the currently logged in users profile from the db'
      }
    },
    {
      method: 'PUT',
      path: '/users/{userId}',
      handler: (request, reply) => {
        delete request.payload.v
        // delete request.payload._id
        Users.update({
          userId: request.params.userId
        }, {
          $set: request.payload
        }, {
          upsert: true
        })
        .exec((err, done) => {
          if (err) {
            return reply(Boom.badRequest('User profile operation error', err))
          }
          reply(done)
        })
      }
    },
    {
      method: 'POST',
      path: '/check-numbers-for-pluggers',
      handler: (request, reply) => {
        let pay = request.payload.map(m => {
          return (m && m.replace) ? m.replace(/\D/g, '').substr(-10) : null
        })
        Users.find({})
        .where('phone').in(pay)
        .lean()
        .exec((err, people) => {
          if (err) {
            return reply(Boom.badRequest('operation error', err))
          }
          reply(people)
        })
      },
      config: {
        validate: {
          payload: Joi.array()
        }
      }
    }
  ])

  next()
}

exports.register.attributes = {
  name: 'users'
}
