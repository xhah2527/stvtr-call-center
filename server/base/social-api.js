const Request = require('request')
const IsOnline = require('./schema/is-online')
const UsersC = require('./schema/user-schema')
const PostModel = require('./schema/post-schema').Post
const Users = require('./users')
const Boom = require('boom')
const Posts = require('./social/posts')
const _ = require('lodash')

const Joi = require('joi')

const Crud = require('../lib/crud')

const stream = require('getstream')
const client = stream.connect('fp3j44zrkhbj', 'sabz57sgem4nafaamvbprmuujwtrs3yg3736x5xpwz7jvk5qp9pk7688cra94hz2')
const debug = require('debug')('em-ninja:socialApi')

class Social {

  /**
   * this function will populate all fields specified
   * by actionMap to the object in initial. The results
   * of passing the initial thru a series of functions
   * which are the values of each key in actionMap
   * @param {Object} actionMap an object which has the
   * names of fields to populate as keys and actions
   * or opertations to carry out on those fields as values
   * @param {Array} initial the initial array/collection
   * whose keys specified in actionMap will be populated
   * by using functions as directed by the values of keys
   * in actionMap
   * @param {Array} result the result of all the operations
   * will be pushed in here
   * @param {function} done the callback function with two
   * arguments passed in node style (err, results).
   */
  populate (actionMap, initial, result, cb) {
    const self = this
    // gather the operations to be carried out.
    // pop the next object,
    // send it thru the each pipes
    // each pip calls next to forward
    // to the next function
    if (!initial.length) return cb(null, result)
    let currentItem = initial.pop()
    let listOfKeysToBePopulated = _.keys(actionMap)
    let listOfAFns = _.values(actionMap)
    let pos = 0
    const doAllFunctions = function () {
      // first functions
      let currentFn = self[listOfAFns.pop()]
      let currentKey = listOfKeysToBePopulated.pop()
      currentFn(currentItem, function (returnedValue) {
        // copy over values
        let extraCurrentItem = {}
        for (let k in currentItem) {
          if (currentItem.hasOwnProperty(k)) {
            extraCurrentItem[k] = currentItem[k]
          }
        }
        currentItem[currentKey] = returnedValue
        if (listOfAFns.length) {
          doAllFunctions()
        } else {
          result.push(currentItem)
          self.populate(actionMap, initial, result, cb)
        }
      })
    }
    doAllFunctions()
  }

  populateUser (currentItem, cb) {
    let actor
    if (typeof currentItem.actor === 'object') {
      actor = currentItem.actor.userId
    }
    if (typeof currentItem.actor === 'string') {
      actor = (currentItem.actor.split(':')[1]) ? currentItem.actor.split(':')[1] : currentItem.actor.split(':')[0]
    }
    UsersC.findOne({
      userId: actor
    })
    .exec((err, userD) => {
      if (err) {
        return cb(err)
      }
      cb(userD)
    })
  }

  populateUsers (listUsers, cb) {
    const self = this
    listUsers.returnedList = listUsers.returnedList || []
    let currentItem = listUsers.lists.pop()
    try {
      currentItem = currentItem.toObject()
    } catch (e) {
      // console.log(e)
      debug(e)
    }
    UsersC.findOne({
      userId: currentItem.userId || currentItem.actor
    })
    .exec((err, userD) => {
      if (err) {
        console.log(err)
      }
      if (userD) {
        if (userD.phone && userD.displayName) {
          currentItem.userData = userD.toObject()
          listUsers.returnedList.push(currentItem)
        }
      }
      if (listUsers.lists.length) {
        self.populateUsers(listUsers, cb)
      } else {
        cb(listUsers.returnedList)
      }
    })
  }

  populatePostObject (currentItem, cb) {
    let origin = currentItem.object
    let id = (origin.split(':')[1]) ? origin.split(':')[1] : origin.split(':')[0]
    PostModel.findOne({
      _id: id
    })
    .exec((err, userD) => {
      if (err) {
        cb(err)
      }
      cb(userD)
    })
  }

  populateOrigins (listPosts, cb) {
    listPosts.returnedList = listPosts.returnedList || []
    let currentItem = listPosts.lists.pop()

    let docId = currentItem.object
    // sometimes, or recently, I started using
    // feedslug:uniqueId as the value of the object
    // key. to be backward compatible.
    if (currentItem.object && currentItem.object.indexOf(':') > -1) {
      docId = currentItem.object.split(':')[1]
    }
    // find the post
    PostModel.findOne({
      _id: docId
    }, 'post_content')
    .lean()
    .exec((err, userD) => {
      if (err) {
        // return reply(Boom.badRequest(err))
        console.log(err)
      }
      if (userD) {
        currentItem.postData = userD.toObject()
        // find any comments for this post
        Posts.find({
          'post_type': 'comment',
          'post_content.parent_post': docId
        })
        .limit(10)
        .lean()
        .exec()
        .then(comments => {
          currentItem.postData.comments = comments
          listPosts.returnedList.push(currentItem)
          if (listPosts.lists.length) {
            populateStories(listPosts, cb)
          } else {
            cb(listPosts.returnedList)
          }
        }, err => {
          console.log(err)
          if (listPosts.lists.length) {
            populateStories(listPosts, cb)
          } else {
            cb(listPosts.returnedList)
          }
        })
      } else {
        if (listPosts.lists.length) {
          populateStories(listPosts, cb)
        } else {
          cb(listPosts.returnedList)
        }
      }
      // if (listPosts.lists.length) {
      //   populateStories(listPosts, cb)
      // } else {
      //   cb(listPosts.returnedList)
      // }
    }, err => {
      console.log(err)
      if (listPosts.lists.length) {
        populateStories(listPosts, cb)
      } else {
        cb(listPosts.returnedList)
      }
    })
  }
}

// Base routes for default index/root path, about page, 404 error pages, and others..
exports.register = function (server, options, next) {
  const api = server.select('api')

  api.realm.modifiers.route.prefix = '/api/v1'
  api.route([
    {
      method: 'PUT',
      path: '/is-online',
      handler: function (request, reply) {
        IsOnline
        .update({
          userId: request.auth.credentials.uid
        }, {
          checkInTime: Date.now()
        }, {
          upsert: true
        })
        .exec((err, updateResult) => {
          if (err) {
            reply(Boom.badRequest('invalid op', err))
          }
          reply()
        })
      },
      config: {
        id: 'is-online'
      }
    },
    {
      method: 'GET',
      path: '/is-online',
      handler: (req, reply) => {
        const social = new Social()
        IsOnline
        .find({
          // TODO: filter by users I follow
        })
        .exec((err, docs) => {
          if (err) {
            reply(Boom.badRequest('invalid op', err))
          }
          if (docs && docs.length) {
            social.populateUsers({lists: docs}, (ud) => {
              reply(ud)
            })
          }
          // reply(docs)
        })
      },
      config: {
      }
    },
    {
      method: 'POST',
      path: '/posts/{postId}/likes',
      handler: (request, reply) => {
        const posts = new Posts(request.auth.credentials.uid)
        let crudDo = new Crud('posts')
        let savedPosts = request.payload
        crudDo.create(savedPosts)
        .then((idDocument) => {
          // the ObjectId of the actual comment
          savedPosts.docId = idDocument._id.toString()
          // use this to send a notification about
          // this comment to the post author
          savedPosts.to = `notification:${savedPosts.post_author}`
          posts.postToFeed(savedPosts)
          .then(done => {
            // no need to wait a callback for the increment.
            crudDo.updatePostProperty(request.params.postId, 'likes')
            .then(done => {
              reply(done)
            }, er => {
              reply(Boom.badRequest(er))
            })
          }, err => {
            reply(err)
          })
          .catch(err => {
            reply(err)
          })
        }, err => {
          reply(Boom.badRequest('invalid op', err))
        })
      },
      config: {
        validate: {
          payload: {
            parent_post: Joi.string(),
            post_author: Joi.string().required(),
            post_type: Joi.string().required()
          }
        }
      }
    },
    /**
     * used to add a comment to a post.
     * could be used to also update a post
     * like when a user changes the text or
     * images for a post / status update
     */
    {
      method: 'POST',
      path: '/posts/{postId}/comments',
      handler: (request, reply) => {
        const posts = new Posts(request.auth.credentials.uid)
        let crudDo = new Crud('posts')
        let savedPosts = request.payload
        crudDo.create(savedPosts)
        .then((idDocument) => {
          // the ObjectId of the actual comment
          savedPosts.docId = idDocument._id.toString()
          // use this to send a notification about
          // this comment to the post author
          savedPosts.to = `notification:${savedPosts.post_author}`
          posts.postToFeed(savedPosts)
          .then(done => {
            reply(done)
          }, err => {
            reply(err)
          })
          .catch(err => {
            reply(err)
          })
        }, err => {
          reply(Boom.badRequest('invalid op', err))
        })
      },
      config: {
        validate: {
          payload: {
            parent_post: Joi.string(),
            comment_body: Joi.string(),
            post_author: Joi.string().required(),
            post_type: Joi.string().required()
          }
        }
      }
    },
    {
      method: 'GET',
      path: '/posts/{postId}/likes',
      handler: (request, reply) => {
        const crudOp = new Crud('posts')
        crudOp.count({
          'post_content.parent_post': request.params.postId,
          post_type: 'like'
        })
        .sort({
          post_date: 1
        })
        .exec((err, c) => {
          if (err) {
            return reply(err)
          }
          if (c.length) {
            return reply(c)
          }
          reply(c)
        })
      }
    },
    {
      method: 'GET',
      path: '/posts/{postId}/comments',
      handler: (request, reply) => {
        const crudOp = new Crud('posts')
        crudOp.collectionInstance.find({
          'post_content.parent_post': request.params.postId,
          post_type: 'comment'
        })
        .sort({
          post_date: 1
        })
        .exec((err, cmts) => {
          if (err) {
            return reply(err)
          }
          if (cmts.length) {
            return reply(cmts)
          }
          reply([])
        })
      }
    },
    /**
     * this will return all the post belonging
     * to a user specified by `request.params.userId`
     */
    {
      method: 'GET',
      path: '/users/{userId}/posts',
      handler: (request, reply) => {
        const social = new Social()
        const posts = new Posts(request.params.userId)
        // const crud = new Crud('posts')
        posts.getFeed()
        .then(acts => {
          let resultCollection = []
          let re = acts.results.filter(function (m) {
            if (m.verb === request.query.posttype) {
              return true
            }
            if (m.verb === 'feed-post') {
              return true
            }
            if (m.verb === 'micro-task') {
              return true
            }
            return false
          })
          social.populate({
            'origin': 'populateUser',
            'object': 'populatePostObject',
            'actor': 'populateUser'
          },
          re,
          resultCollection,
          function (err, newObject) {
            if (err) {
              return reply(err)
            }
            reply(newObject)
            // reply(_.map(newObject, 'object'))
          })
        }, err => {
          reply(Boom.badRequest('unable to complete', err))
        })
        .catch(err => {
          reply(Boom.badRequest('unable to complete', err))
        })
      }
    },
    {
      method: 'GET',
      path: '/feeds',
      handler: (request, reply) => {
        const posts = new Posts(request.auth.credentials.uid)
        posts.myNotificationFeed.get()
        .then(f => {
          reply(
            {
              activity: f.results,
              token: posts.myUserFeed.token
            })
        }, err => {
          reply(Boom.badRequest('something went wrong'))
        })
      }
    },
    {
      method: 'GET',
      path: '/posts/{postId?}',
      handler: (request, reply) => {
        const social = new Social()
        const posts = new Posts(request.auth.credentials.uid)
        // const crud = new Crud('posts')
        posts.getFeed()
        .then(acts => {
          let resultCollection = []
          let re = acts.results.filter(function (m) {
            if (m.verb === request.query.posttype) {
              return true
            }
            if (m.verb === 'feed-post') {
              return true
            }
            if (m.verb === 'micro-task') {
              return true
            }
            return false
          })
          social.populate({
            'origin': 'populateUser',
            'object': 'populatePostObject',
            'actor': 'populateUser'
          },
          re,
          resultCollection,
          function (err, newObject) {
            if (err) {
              return reply(err)
            }
            reply(newObject)
            // reply(_.map(newObject, 'object'))
          })
        }, err => {
          reply(Boom.badRequest('unable to complete', err))
        })
        .catch(err => {
          reply(Boom.badRequest('unable to complete', err))
        })
      }
    },
    /**
     * removes an activity from the currentUsers list of
     * activities on currentUsers activity feed. This
     * does not attempt to remove the actual post or entry
     * from the database.
     */
    {
      method: 'DELETE',
      path: '/activities/{actId}',
      handler: (request, reply) => {
        let me = new Posts(request.auth.credentials.uid)
        // todo: Add record of this transaction to mongodb
        me.myUserFeed.removeActivity(request.params.actId)
        .then(() => {
          reply()
        }, err => {
          reply(Boom.badRequest(err))
        })
      }
    },
    {
      method: 'POST',
      path: '/posts',
      handler: (request, reply) => {
        const posts = new Posts(request.auth.credentials.uid)
        let crudDo = new Crud('posts')
        let savedPosts = request.payload
        savedPosts.post_author = savedPosts.author
        crudDo.create(savedPosts)
        .then((idDocument) => {
          request.payload.docId = idDocument._id
          posts.postToFeed(savedPosts)
          .then((s) => {
            reply(s)
          }, err => {
            reply(Boom.badRequest(err))
          })
        }, err => {
          reply(Boom.badRequest('invalid op', err))
        })
      },
      config: {
        validate: {
          payload: {
            author: Joi.string(),
            body: Joi.string().min(1),
            comments: Joi.object(),
            postDate: Joi.date(),
            attachments: [Joi.array()],
            post_type: Joi.string().required()
          }
        }
      }
    },
    {
      method: 'POST',
      path: '/follow/{feedname}/{id}',
      handler: (request, reply) => {
        // const f = new Posts(request.auth.credentials.uid)
        let me = new Posts(request.auth.credentials.uid)
        // todo: Add record of this transaction to mongodb
        me.myUserFeed.follow('user', request.params.id)
        .then(() => {
          me.myUserFeed.add_activity({
            'actor': request.auth.credentials.uid,
            'verb': 'post',
            // 'object': 'post:10',
            // 'foreign_id': 'post:10',
            'to': [`notification:${request.params.id}`],
            'message': `Hi @${request.auth.credentials.displayName} is now following you.`
          });
          reply()
        }, err => {
          reply(Boom.badRequest('invalid op', err))
        })
      }
    },
    {
      method: 'DELETE',
      path: '/follow/{feedname}/{id}',
      handler: (request, reply) => {
        // const f = new Posts(request.auth.credentials.uid)
        let me = new Posts(request.auth.credentials.uid)
        // todo: Add record of this transaction to mongodb
        me.myUserFeed.unfollow('user', request.params.id)
        .then(() => {
          reply()
        }, err => {
          reply(Boom.badRequest('invalid op', err))
        })
      }
    }
  ])

  next()
}

exports.register.attributes = {
  name: 'social'
}
