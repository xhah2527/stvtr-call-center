

exports.register = function (server, options, next) {
  const api = server.select('api')

  api.realm.modifiers.route.prefix = '/api/v1'
  api.route([
    {
      method: 'GET',
      path: '/feeds/ads/{id?}',
      handler: (request, reply) => {
        reply(require('./ads.json'))
      }
    }
  ])
  next()
}

exports.register.attributes = {
  name: 'ads'
}
