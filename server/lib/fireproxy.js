const admin = require('firebase-admin')
const path = require('path')
const Boom = require('boom')

admin.initializeApp({
  credential: admin.credential.cert(path.resolve('./server/keys/emplug-firebase.json')),
  databaseURL: 'https://mobile-2f635.firebaseio.com'
})

module.exports = (request, reply) => {
  const req = request.raw.req
  // if (req.url === '/api/v1/submitfile') {
  //   return reply.continue({credentials: {user: 'incomingMobileUpload'}})
  // }
  admin.auth().verifyIdToken(req.headers.authorization)
      .then((decodedToken) => {
        // request.user = decodedToken
        // // TODO:: this is scotch tape
        // request.user._id = decodedToken.uid
        reply.continue({credentials: decodedToken})
      }).catch((error) => {
        reply(Boom.unauthorized(error))
      })
}
