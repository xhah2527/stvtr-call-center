const mongoose = require('mongoose')
const PostsModel = require('../base/schema/post-schema').Post
class Crud {
  constructor (collectionName) {
    if (!collectionName) {
      throw new Error('Cannot instantiate class without collectionName parameter / argument')
    }
    this.collectionName = collectionName
    this.collectionInstance = mongoose.model(collectionName)
  }
  /**
   * creates a document for the specified
   * collection
   */
  create (doc) {
    const post = new PostsModel()
    post.post_title = doc.title || 'No title'
    post.post_author = doc.post_author
    post.post_content = doc
    post.post_status = 'published'
    post.post_type = doc.post_type
    post.attachements = doc.attachements
    return post.save()
  }

  /**
   * Post have certain properties that allow certain actions
   * to be carried out on the post
   * e.g. Posts can be liked.
   * The best way to acheive this is to create a Post[action]
   * property on the Post Model.
   * This update method will perform legal actions on a post's
   * property
   * @param {String} postId specifies the id for the post 
   * document this action will executed on.
   * @param {String} prop the property to carry out the action 
   * `op` on.
   * @param {String} op the kind of action we wanna do
   * @returns {Promise}
   */
  updatePostProperty (postId, prop, op = 'inc') {
    const self = this
    return new Promise((resolve, reject) => {
      self.collectionInstance.update({
        _id: postId
      }, {
        '$inc': {
          likes: 1
        }
      })
      .exec((err, done) => {
        if (err) {
          return reject(err)
        }
        resolve(done)
      })
    })
  }

  /**
   * Deletes or removes a document from a collection
   * on the database
   *
   * @param {any} docId the document Id for the document being
   * removed
   * @param {any} collectionName the collection to carry out this
   * operation on
   * @returns
   *
   * @memberOf Crud
   */
  deleteOne (docId) {
    const collection = mongoose.model(this.collectionName)
    return collection.remove({
      _id: docId
    })
  }
  /**
   * this method will change the status 
   * of a post to 'deleted'. When successful
   * api queries to retrieve posts will omit
   * this post.
   */
  deletePost (postId) {
    const collection = mongoose.model(this.collectionName)
    return collection.update({
      _id: postId
    }, {
      post_status: 'deleted'
    })
  }
  
  /**
   * Attempts to perform a document
   * update or create one using the
   * upsert option
   *
   * @param {any} docId
   * @param {any} updateDoc
   * @param {boolean} [upsert=false]
   * @returns
   *
   * @memberOf Crud
   */
  updateOne (docId, updateDoc, upsert = false) {
    return mongoose.model(this.collectionName)
    .update({
      _id: docId
    }, {
      $set: updateDoc
    }, {
      upsert: upsert
    })
    .exec()
  }
  /**
   * Will return the object / data for
   * one document saved on the database
   *
   * @param {any} params expects a document ObjectId
   * as a key / property of this hash
   * @returns
   *
   * @memberOf Crud
   */
  readOne (params, populate = {}) {
    let mode = mongoose.model(this.collectionName)
    mode.findOne(params)
    if (populate.path) {
      mode.populate(populate)
    }
    return mode
  }
  readAll (params, populate = {}) {    
    let mode = this.collectionInstance
    mode.find(params).limit(10)
    if (populate.path) {
      mode.populate(populate)
    }
    // mode.limit(20)
    return mode
  }
  count (params) {
    return mongoose.model(this.collectionName)
    .count(params)
  }
}
module.exports = Crud
