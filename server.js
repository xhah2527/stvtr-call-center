/**
* Dependencies.
*/
var Hapi = require('hapi')
var corsHeaders = require('hapi-cors-headers')
const debug = require('debug')('em-ninja:server')
const db = require('./server/lib/db')
// const Inert = require('inert')
// const path = require('path')

// Create a new server
var server = new Hapi.Server()

if (!process.env.NINJAPORT && !process.env.PORT) throw new Error('missing a required environment variable, NINJAPORT')

// Setup the server with a host and port
const port = process.env.PORT || process.env.NINJAPORT
server.connection({
  port: parseInt(port, 10),
  host: process.env.IP || '0.0.0.0',
  labels: 'api',
  routes: { cors: true } 
  // routes: {
  //   'apiAssets': {
  //     relativeTo: path.join(__dirname, 'server', 'assets')
  //   }
  // }
})

// Setup the views engine and folder
server.views({
  engines: {
    html: require('swig')
  },
  path: './server/views'
})


// Export the server to be required elsewhere.
module.exports = server

const scheme = function (server, options) {
  return {
    api: {
      settings: {
        x: 5
      }
    },
    // authenticate: require('./server/lib/fireproxy')
    // authenticate: (request, reply) => {
    //   reply.continue()
    // }
  }
}

// server.auth.scheme('fireproxy', scheme)
// server.auth.strategy('fireman', 'fireproxy')
// server.auth.default('fireman')

/*
    Load all plugins and then start the server.
    First: community/npm plugins are loaded
    Second: project specific plugins are loaded
 */

// server.register(Inert, () => {})

server.register([
  // {
  //   register: require('inert')
  // },
  {
    register: require('good'),
    options: {
      opsInterval: 5000,
      reporters: [{
        reporter: require('good-console'),
        args: [{ ops: '*', request: '*', log: '*', response: '*', 'error': '*' }]
      }]
    }
  },
  {
    register: require('hapi-assets'),
    options: require('./assets.js')
  },
  {
    register: require('hapi-named-routes')
  },
  {
    register: require('hapi-cache-buster')
  },
  {
    register: require('./server/assets/index.js')
  },
  {
    register: require('./server/base/index.js')
  }
], function () {
  server.ext('onPreResponse', corsHeaders)

    // Start the server
  db.open()
  .then(() => {
    server.select('api').start(function () {
          // Log to the console the host and port info
      console.log('Server started at: ' + server.info.uri)
    }, err => {
      debug(err)
    })
  })
})
